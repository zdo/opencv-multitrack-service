FROM jjanzic/docker-python3-opencv:contrib

WORKDIR /app

COPY ./src/requirements.txt ./
RUN pip install -r requirements.txt

COPY ./src/ ./

ENTRYPOINT bash ./start_server
