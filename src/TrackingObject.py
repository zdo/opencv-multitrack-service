class TrackingObject:
    def __init__(self, x=0, y=0, w=0, h=0):
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def from_bbox(self, bbox):
        self.x = bbox[0]
        self.y = bbox[1]
        self.w = bbox[2]
        self.h = bbox[3]

    def to_bbox(self):
        return self.x, self.y, self.w, self.h
        