from flask import Flask, request, make_response
from flask_cors import CORS

from TrackingSession import TrackingSession
from TrackingObject import TrackingObject

import os
import json
import uuid
import cv2
from decouple import config

app = Flask(__name__)
CORS(app)

time_to_become_obsolete = config('TIME_TO_BECOME_OBSOLETE', default=60 * 3, cast=int) # seconds
port = config('PORT', default=5000)

# ---

g = app.app_context().g

if not hasattr(g, 'sessions'):
    g.sessions = {}

def delete_obsolete_sessions():
    to_remove = []
    for session_id in g.sessions:
        session = g.sessions[session_id]
        dt = session.get_seconds_since_last_use()
        if dt > time_to_become_obsolete:
            to_remove.append(session_id)
    for id in to_remove:
        del g.sessions[id]

def save_file(request, key='file'):
    file = request.files[key]
    extension = os.path.splitext(file.filename)[1]
    f_name = str(uuid.uuid4()) + extension
    full_path = os.path.join('/tmp', f_name)
    file.save(full_path)
    return full_path

def get_session(request):
    session_id = None
    if 'session' in request.form:
        session_id = request.form['session']
    
    session = None
    if session_id is not None and session_id in g.sessions:
        session = g.sessions[session_id]
    return session

def parse_objects(src):
    objects_data = json.loads(src)
    objects = []
    for o0 in objects_data:
        o = TrackingObject()
        o.from_bbox(o0)
        objects.append(o)
    return objects

def process_session(request):
    image_path = save_file(request)
    image = cv2.imread(image_path)

    answer = {}
    session = get_session(request)
    if session is None:
        wanted_id = None
        if 'session' in request.form:
            wanted_id = request.form['session']

        objects = parse_objects(request.form['objects'])
        session = TrackingSession(image, objects, id=wanted_id)
        g.sessions[session.id] = session
    else:
        session.track(image)
        
        tracked = []
        for o in session.objects:
            tracked.append(o.to_bbox())
        answer['objects'] = tracked

    answer['session'] = session.id

    os.unlink(image_path)
    return json.dumps(answer)

def process_session_oneshot(request):
    image_path = save_file(request, 'file')
    image_path_2 = save_file(request, 'file2')

    image = cv2.imread(image_path)
    image_2 = cv2.imread(image_path_2)

    answer = {}
    objects = parse_objects(request.form['objects'])
    session = TrackingSession(image, objects)

    session.track(image_2)
        
    tracked = []
    for o in session.objects:
        tracked.append(o.to_bbox())
    answer['objects'] = tracked

    os.unlink(image_path_2)
    os.unlink(image_path)

    return json.dumps(answer)


@app.route('/track', methods=['POST'])
def get_detection():
    res = process_session(request)
    delete_obsolete_sessions()
    return res

@app.route('/track-oneshot', methods=['POST'])
def get_detection_single():
    res = process_session_oneshot(request)
    return res

@app.route('/selective-search', methods=['POST'])
def selective_search():
    #cv2.setUseOptimized(True)
    cv2.setNumThreads(4)

    image_path = save_file(request, 'file')
    im = cv2.imread(image_path)

    ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
    ss.setBaseImage(im)

    fast = True
    if 'hq' in request.form:
        fast = False
    if fast:
        ss.switchToSelectiveSearchFast()
    else:
        ss.switchToSelectiveSearchQuality()

    rects = ss.process()

    imOut = im.copy()

    for i, rect in enumerate(rects):
        x, y, w, h = rect
        cv2.rectangle(imOut, (x, y), (x+w, y+h), (0, 255, 0), 1, cv2.LINE_AA)

    retval, buffer = cv2.imencode('.png', imOut)
    response = make_response(buffer.tobytes())
    return response

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=port, debug=False)
