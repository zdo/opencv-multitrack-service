from TrackingObject import TrackingObject
import cv2 as cv
import uuid
from datetime import datetime

class TrackingSession:
    def __init__(self, image, objects, id=None):
        if id and len(id) > 0:
            self.id = id
        else:
            self.id = str(uuid.uuid4())
            
        self.last_used = datetime.now()
        self.tracker = cv.MultiTracker_create()
        self.objects = objects

        for obj in self.objects:
            ok = self.tracker.add(cv.TrackerMIL_create(), image, obj.to_bbox())
            if not ok:
                raise Exception("Can't create tracker for object")

        print('Session {} created'.format(self.id))

    def __del__(self):
        print('Session {} died'.format(self.id))

    def track(self, image):
        self.last_used = datetime.now()

        ok, boxes = self.tracker.update(image)
        if not ok:
            raise Exception("Tracker update error")

        for i in range(0, len(boxes)):
            self.objects[i].from_bbox(boxes[i])

    def get_seconds_since_last_use(self):
        now = datetime.now()
        dt = (now - self.last_used).total_seconds()
        return dt
