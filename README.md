# Multitracking webservice

This is an OpenCV's MultiTracker wrapped up in web-service.

You can use it by sending POST requests to `/track`.

## Configuration

- `-e PORT=5000`: TCP-port for web-service

## Usage

### A lot of images

1. Create new track session:

```bash
$ curl -F 'file=@image0001.jpg' -F 'objects=[[10,10,100,100]]' 'localhost:5001/track'
{"session": "027fd977-887f-4194-a8b6-f8ed93844d34"}
$ 
```

- `file` should contain image
- `objects` is an JSON-array of objects' coordinates in form `[x, y, w, h]`

Response will contain new created session's id.

By the way, you can specify your wanted session id by sending the same request with `-F 'session=MYCUSTOMID'`.

2. Put new image into the tracker.

```bash
$ curl -F 'file=@image0002.jpg' -F 'objects=[[15,15,100,100]]' -F 'session=027fd977-887f-4194-a8b6-f8ed93844d34' 'localhost:5001/track'
{"objects": [[10.0, 10.0, 100.0, 100.0]], "session": "027fd977-887f-4194-a8b6-f8ed93844d34"}
```

Response contains calculated coordinates of tracked objects.

### Single shot tracking

In case you need to track objects only between two images, you can use the similar mechanism but with sending also second image in `file2` and no `session`:

```
POST /track-oneshot
file=<image>
objects=<objects for image>
file2=<image2>
```

That request will return the same format as described above.
